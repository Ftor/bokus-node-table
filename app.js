const express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    hbs = require("express-handlebars"),
    axios = require('axios');

const app = express();

app.set('port', process.env.VIEWPORT || 3000);

app.use(logger('dev'));

app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'main',
    helpers: {
        json:function (context) {
            return JSON.stringify(context);
        }
    }
}));
app.set('view engine', 'hbs');

app.use(logger('dev'));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', async function (req, res) {

    const getData = async url => {
        try {
            const response = await axios.get(url);
            const data = response.data;
            return data

        } catch (error) {
            console.log(error);
        }
    };

    const url = "http://z.bokus.ru/user.json";
    let data = await getData(url);

    res.render('index', {data:data});
});


app.use((req, res, next) => {
    res.status(404).render('404');
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.status = err.status || 500;
    res.status(err.status || 500).render('500');
});

app.listen(app.get('port'), () => {
    console.log(`Server listening on http://localhost:${app.get('port')};`);
});
